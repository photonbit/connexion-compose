#!/usr/bin/env bash

reqs:
	pip-compile --upgrade --output-file requirements/base.txt requirements/base.in
	pip-compile --upgrade --output-file requirements/dev.txt requirements/dev.in
	pip-compile --upgrade --output-file requirements/test.txt requirements/test.in

test:
	pytest ./test

dist:
	python setup.py sdist

pypi:
	twine upload ./dist/connexion-compose-*.tar.gz

black:
	black -l 120 --target-version py37 connexion_compose test

lint:
	pylint connexion_compose test

mypy:
	mypy --config-file mypy.ini -p connexion_compose

.PHONY: reqs black lint mypy test dist pypi
