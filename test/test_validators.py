from decimal import Decimal

import pytest

from connexion_compose import validators


@pytest.mark.parametrize(
    "value, expected", [("EUR", True), ("ABC", True), ("eur", False), ("AB", False), ("ABCD", False), (1, False)]
)
def test_currency_code(value, expected):
    assert validators.is_iso_4217(value) == expected


@pytest.mark.parametrize(
    "value, expected", [(Decimal("20.1"), True), ("20.1", True), (20.1, False), (20, False), ("nonsense", False)]
)
def test_decimal(value, expected):
    assert validators.is_decimal(value) == expected


@pytest.mark.parametrize(
    "value, expected",
    [
        ("2018-10-02T10:00:00-05:00", True),
        ("2018-10-02t10:00:00+05:00", True),
        ("2018-10-02T15:00:00Z", True),
        ("2018-10-02T15:00:00.05Z", True),
        ("2018-10-02 15:00:00-05:00", False),
        (173_486_354, False),
        ("nonsense", False),
    ],
)
def test_timestamp(value, expected):
    assert validators.is_rfc_3339(value) == expected


@pytest.mark.parametrize(
    "value, expected",
    [
        ("https://mailing.list.example.com:10040/foo/bar/index.aspx/?eenie=meenie&manie=moe#anchor", True),
        ("http://www.example.com", True),
        ("http://www.example.com:[123", False),
        ("http://www.example.com:invalid", False),
        ("http://", False),
        ("www.example.com", False),
        ("/foo/bar", False),
        ("C:/My Documents", False),
        (173_486_354, False),
        ("nonsense", False),
    ],
)
def test_url(value, expected):
    assert validators.is_url(value) == expected


@pytest.mark.parametrize(
    "value, expected",
    [
        ("/foo/bar/baz/index.aspx/?eenie=meenie&manie=moe#anchor", True),
        ("/foo/bar/baz", True),
        ("foo/bar/baz/", True),
        ("https://www.example.com:10040", False),
        ("http://www.example.com", False),
        ("C:/My Documents", False),
        (173_486_354, False),
    ],
)
def test_path(value, expected):
    assert validators.is_path(value) == expected


@pytest.mark.parametrize(
    "value, expected",
    [
        ("12345678-1234-1234-1234-123456789abc", True),
        ("12345678-1234-1234-1234-123456789ABC", True),
        ("12345678-1234-1234-1234-123456789ab", False),
        ("12345678-1234-1234-1234-123456789abcd", False),
        ("123*5678-1234-1234-1234-123456789abc", False),
        ("nonsense", False),
        (173_486_354, False),
    ],
)
def test_uuid(value, expected):
    assert validators.is_uuid(value) == expected
