from pathlib import Path

import pytest

from connexion_compose import compile_schema


def test_compiler():
    schema = compile_schema(Path(__file__).parent / "schemas")

    expected = {
        "swagger": "3.0",  # Overriden
        "info": {  # Overriden
            "title": "Test title",
            "description": "Test description",
            "contact": {"name": "Desdemona"},
            "version": "1.2.3",
            "x-visibility": "unlisted",
        },
        "basePath": "/",
        "schemes": ["https"],
        "consumes": ["application/json"],
        "produces": ["application/json"],
        "definitions": {
            "Currency": {"type": "string", "pattern": "^[0-9]{3}$"},  # Overriden
            "Decimal": {"type": "string", "format": "decimal"},
            "Path": {"type": "string", "format": "path"},
            "Timestamp": {"type": "string", "format": "rfc_3339"},
            "URL": {"type": "string", "format": "url"},
            "UUID": {"type": "string", "format": "uuid"},
            "CreatedResponse": {  # Added
                "description": "My Created Response",
                "properties": {
                    "path": {"description": "The relative path of the created resource", "$ref": "#/definitions/Path"}
                },
                "required": ["path"],
                "title": "CreatedResponse",
                "type": "object",
            },
        },
        "x": {"y": {"foo": {"bar": "baz"}}},  # Added
    }

    assert schema == expected


def test_compiler_no_spec_dir():
    schema = compile_schema()

    expected = {
        "swagger": "2.0",
        "info": {
            "title": "Please add a title",
            "description": "Please add a description",
            "contact": {"name": "Please add a contact name"},
            "version": "0.0.0",
            "x-visibility": "unlisted",
        },
        "basePath": "/",
        "schemes": ["https"],
        "consumes": ["application/json"],
        "produces": ["application/json"],
        "definitions": {
            "Currency": {"type": "string", "format": "iso_4217"},
            "Decimal": {"type": "string", "format": "decimal"},
            "Path": {"type": "string", "format": "path"},
            "Timestamp": {"type": "string", "format": "rfc_3339"},
            "URL": {"type": "string", "format": "url"},
            "UUID": {"type": "string", "format": "uuid"},
        },
    }

    assert schema == expected


def test_compiler_nonexistent_dir():
    with pytest.raises(ValueError):
        compile_schema("nonexistent/directory")
