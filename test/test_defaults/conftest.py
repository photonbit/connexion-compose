from pathlib import Path

import connexion
import pytest

from connexion_compose import compile_schema


@pytest.fixture
def app():
    app = connexion.FlaskApp(__name__)
    app.add_api(
        compile_schema(Path(__file__).parent / "schemas"),
        validate_responses=True,
        strict_validation=True,
        resolver=connexion.RestyResolver("test.test_defaults.handlers"),
    )
    return app.app
